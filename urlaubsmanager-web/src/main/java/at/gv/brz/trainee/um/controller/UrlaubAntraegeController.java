package at.gv.brz.trainee.um.controller;

import static at.gv.brz.trainee.um.utility.Constants.NAVIGATION_ON_FAILURE;
import static at.gv.brz.trainee.um.utility.Constants.NAVIGATION_ON_SUCCESS;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.ListDataModel;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.Urlaub;
import at.gv.brz.trainee.um.model.User;
import at.gv.brz.trainee.um.model.VTeam;
import at.gv.brz.trainee.um.service.impl.UrlaubServiceJPA;

/**
 * Kontrolliert den Antrag der Urlaubserstellung.
 * 
 * @author toemoes
 *
 */
public class UrlaubAntraegeController implements Serializable {

    private static final long serialVersionUID = 1L;

    private UrlaubServiceJPA urlaubService;

    private ListDataModel<Urlaub> urlaube;

    private Urlaub currentUrlaub;

    private User currentUser;

    private VTeam currentVTeam;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    private boolean genehmigt;

    private boolean abgelehnt;

    public String showUrlaubAntraege() {
        genehmigt = false;
        abgelehnt = false;
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Genehmigt den Urlaubsantrag.
     * 
     * @return Fehler oder Erfolg Text als String.
     */
    public String genehmigen() {
        genehmigt = false;
        abgelehnt = false;
        try {
            urlaubService.setUrlaubgnehmigt(currentUrlaub);
            genehmigt = true;
        } catch (ServiceException e) {
            e.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }
        return NAVIGATION_ON_SUCCESS;
    }

    public void urlaubFuerDialog() {
        currentUrlaub = urlaube.getRowData();
    }

    /**
     * Lehnt den Urlaubsantrag ab.
     * 
     * @return success oder failure as String
     */
    public String ablehnen() {
        genehmigt = false;
        abgelehnt = false;
        try {
            urlaubService.setUrlaubabgelehnt(currentUrlaub);
            abgelehnt = true;
        } catch (ServiceException e) {
            e.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Liefert das Urlaub Start Date formattiert zurück.
     * 
     * @return Formattierte Date als String.
     * @autor gaalcs
     */
    public String getStartDate() {
        return dateFormat.format(currentUrlaub.getStartDatum());
    }

    /**
     * Liefert das Urlaub End Date formattiert zurück.
     * 
     * @return Formattierte Date als String.
     * @autor gaalcs
     */
    public String getEndDate() {
        return dateFormat.format(currentUrlaub.getEndDatum());
    }

    /**
     * Überprüft, ob sich die Daten in einer Liste überscheiden. Es wird jedes Datum einer Reihe einzeln überprüft
     * und mit den Daten der Liste verglichen.
     * 
     * @return true, wenn das Datum überschneidet ein anderes Datum.
     * @throws ServiceException falls etwas mit Service nicht passt.
     * @author gaalcs
     */
    public boolean isUrlaubUeberschneidet() throws ServiceException {
        Urlaub urlaub = urlaube.getRowData();
        long urlaubStartDatum = urlaub.getStartDatum().getTime();
        long urlaubEndDatum = urlaub.getEndDatum().getTime();
        List<Urlaub> hilfliste = getOffeneUrlaubeSortiertByDate();
        for (int i = 0; i < hilfliste.size(); i++) {
            if (urlaub.getId() == hilfliste.get(i).getId()) {
                continue;
            }
            long listStartDatum = hilfliste.get(i).getStartDatum().getTime();
            long listEndDatum = hilfliste.get(i).getEndDatum().getTime();
            if ((urlaubStartDatum >= listStartDatum && urlaubStartDatum <= listEndDatum)
                || (urlaubEndDatum >= listStartDatum && urlaubEndDatum <= listEndDatum)
                || (urlaubStartDatum < listStartDatum && urlaubEndDatum > listEndDatum)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Holt eine Urlaubliste nach VTeam und User, wenn das Status OFFEN ist von der Datenbank und sortiert die Liste
     * nach Start Datum.
     * 
     * @return Eine Liste von Urlaube nach Star Datum sortiert.
     * @throws ServiceException falls etwas mit Service nicht passt.
     * @author gaalcs
     */
    public List<Urlaub> getOffeneUrlaubeSortiertByDate() throws ServiceException {
        List<Urlaub> urlaublist = urlaubService.getUrlaubListOffen(currentVTeam, currentUser);
        return urlaublist.stream().sorted((u1, u2) -> u1.getStartDatum().compareTo(u2.getStartDatum()))
                         .collect(Collectors.toList());
    }

    public UrlaubServiceJPA getUrlaubService() {
        return urlaubService;
    }

    public void setUrlaubService(UrlaubServiceJPA urlaubService) {
        this.urlaubService = urlaubService;
    }

    public ListDataModel<Urlaub> getUrlaube() {
        try {
            urlaube = new ListDataModel<>(getOffeneUrlaubeSortiertByDate());

        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return urlaube;
    }

    public void setUrlaube(ListDataModel<Urlaub> urlaube) {
        this.urlaube = urlaube;
    }

    public Urlaub getCurrentUrlaub() {
        return currentUrlaub;
    }

    public void setCurrentUrlaub(Urlaub currentUrlaub) {
        this.currentUrlaub = currentUrlaub;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public VTeam getCurrentVTeam() {
        return currentVTeam;
    }

    public void setCurrentVTeam(VTeam currentVTeam) {
        this.currentVTeam = currentVTeam;
    }

    public boolean isGenehmigt() {
        return genehmigt;
    }

    public void setGenehmigt(boolean genehmigt) {
        this.genehmigt = genehmigt;
    }

    public boolean isAbgelehnt() {
        return abgelehnt;
    }

    public void setAbgelehnt(boolean abgelehnt) {
        this.abgelehnt = abgelehnt;
    }

}