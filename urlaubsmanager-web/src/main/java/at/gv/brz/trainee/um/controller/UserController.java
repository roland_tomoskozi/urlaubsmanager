package at.gv.brz.trainee.um.controller;

import static at.gv.brz.trainee.um.utility.Constants.NAVIGATION_ON_FAILURE;
import static at.gv.brz.trainee.um.utility.Constants.NAVIGATION_ON_SUCCESS;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.User;
import at.gv.brz.trainee.um.model.enums.Rolle;
import at.gv.brz.trainee.um.service.impl.UserServiceJPA;
import at.gv.brz.trainee.um.utility.PasswortHasher;

/**
 *
 * Diese Klasse enthält Methoden zur Verarbeitung von Anforderungen zum Login, Abmelden, Speichern, Aktualisieren,
 * Löschen und Auflisten des Users.
 *
 * 
 * @author muradd, Kommentaruebersetzung: janua
 *
 */
public class UserController implements Serializable {
    private static final long serialVersionUID = 1L;

    private UserServiceJPA userService;

    private User currentUser = new User();

    private ListDataModel<User> users;

    private boolean wrongUser;

    private boolean berechtigt;

    /**
     * login() holt von userService mit der email und passwort einen User, wenn User nicht null ist, setzt currentUser
     * in MeineVTeamsController auf User und navigiert auf meineVTeamList.xhtml Wenn User null ist, bleibt auf
     * login.xhtml Seite, mit einer Fehlermeldung.
     * 
     * @return success oder failure as String
     * 
     * @author gaalcs
     */
    public String login() {
        wrongUser = false;
        String email = "";
        String hashPasswort = "";
        PasswortHasher ph = new PasswortHasher();
        try {
            if (currentUser.getEmail() != null && currentUser.getPasswort() != null) {
                hashPasswort = ph.genenerateMD5Passwort(currentUser.getPasswort());
                email = currentUser.getEmail();
                currentUser = userService.getUserByEmailAndPass(currentUser.getEmail(), hashPasswort);
            }

            if (currentUser != null) {
                FacesContext ctx = FacesContext.getCurrentInstance();
                MeineVTeamsController vtc = ctx.getApplication().evaluateExpressionGet(ctx, "#{meineVTeamsController}",
                                                                                       MeineVTeamsController.class);
                vtc.setCurrentUser(currentUser);
                vtc.setBerechtigt(validateRolle());
                /**
                 * Navigieren zu meineVteams.xhtml
                 */
                return NAVIGATION_ON_SUCCESS;
            } else {
                wrongUser = true;
                currentUser = new User();
                currentUser.setEmail(email);
                /**
                 * Navigieren zurück zu login.xhtml
                 */
                return NAVIGATION_ON_FAILURE;
            }
        } catch (ServiceException e) {
            e.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }

    }

    /**
     * Methode abmelden() wird immer aufgerufen, wenn jemand sich abmelden will. Setzt User auf new User, navigiert auf
     * login.xhtml Seite
     * 
     * @return NAVIGATION_ON_SUCCESS.
     * @author gaalcs
     */
    public String abmelden() {
        currentUser = new User();
        FacesContext ctx = FacesContext.getCurrentInstance();
        MeineVTeamsController mvtc = ctx.getApplication().evaluateExpressionGet(ctx, "#{meineVTeamsController}",
                                                                                MeineVTeamsController.class);
        mvtc.setCurrentUser(null);

        UrlaubAntraegeController utc = ctx.getApplication().evaluateExpressionGet(ctx, "#{urlaubAntraegeController}",
                                                                                  UrlaubAntraegeController.class);
        utc.setCurrentUser(null);
        VTeamController vtc =
                ctx.getApplication().evaluateExpressionGet(ctx, "#{vTeamController}", VTeamController.class);
        vtc.setCurreUser(null);

        UrlaubController uc =
                ctx.getApplication().evaluateExpressionGet(ctx, "#{urlaubController}", UrlaubController.class);
        uc.setCurrentUser(null);
        /**
         * Navigieren zu login.xhtml
         */
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Methode showLogin() wird zuerst beim logging aufgerufen.
     * 
     * @return NAVIGATION_ON_SUCCESS
     * @autor gaalcs
     */
    public String showLogin() {
        wrongUser = false;
        currentUser = new User();
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Methode: ruft add() Methode von UserService auf. Fuegt den aktuellen User hinzu.
     * 
     * @return NAVIGATION_ON_SUCCESS wenn add() funktioniert, sonst NAVIGATION_ON_FAILURE
     **/
    public String save() {
        try {
            userService.addUser(currentUser);
        } catch (ServiceException se) {
            se.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Methode: ruft upate() Methode von UserService auf. Aktualisiert den aktuellen User.
     *
     * @return NAVIGATION_ON_SUCCESS wenn update() funktioniert, sonst NAVIGATION_ON_FAILURE
     **/
    public String update() {
        try {
            userService.updateUser(currentUser);
        } catch (ServiceException se) {
            se.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Methode: verknuepft den aktuellen User mit der korrekten Zeile.
     *
     * @return NAVIGATION_ON_SUCCESS wenn showEdit() funktioniert, sonst NAVIGATION_ON_FAILURE
     **/
    public String showEdit() {
        try {
            currentUser = users.getRowData();
        } catch (FacesException fe) {
            fe.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Methode: ruft Methode delete() von UserService auf. Diese setzt Variable deleted von User auf true damit User
     * nicht komplett aus der Datenbank geloescht wird. Verknuepft den aktuellen User mit der korrekten Zeile.
     * Geloeschte User werden in der Ausgabe nicht mehr angezeigt.
     * 
     * @return NAVIGATION_ON_SUCCESS wenn delete() funktioniert, sonst NAVIGATION_ON_FAILURE
     **/
    public String delete() {
        currentUser = users.getRowData();
        try {
            userService.deleteUser(currentUser);
        } catch (ServiceException se) {
            se.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Methode: setzt aktuellen User zurueck.
     *
     * @return NAVIGATION_ON_SUCCESS
     **/
    public String showInsert() {
        currentUser = new User();
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Schaut, ob der User berechtigt ist, die Antragsliste zu sehen oder nicht.
     * 
     * @return true,false
     */
    public boolean validateRolle() {
        if (currentUser.getRolle().equals(Rolle.PM) || currentUser.getRolle().equals(Rolle.PL)
            || currentUser.getRolle().equals(Rolle.TL) || currentUser.getRolle().equals(Rolle.ADMIN)) {
            setBerechtigt(true);
            return berechtigt;
        } else {
            setBerechtigt(false);
            return berechtigt;
        }
    }

    /*
     * **************** Getters & Setters ************************
     */

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public ListDataModel<User> getUsers() throws ServiceException {
        this.users = new ListDataModel<>(userService.getAllUser());
        return users;
    }

    public void setUsers(ListDataModel<User> users) {
        this.users = users;
    }

    public UserServiceJPA getUserService() {
        return userService;
    }

    public void setUserService(UserServiceJPA userService) {
        this.userService = userService;
    }

    public boolean isWrongUser() {
        return wrongUser;
    }

    public void setWrongUser(boolean wrongUser) {
        this.wrongUser = wrongUser;
    }

    public boolean isBerechtigt() {
        return berechtigt;
    }

    public void setBerechtigt(boolean berechtigt) {
        this.berechtigt = berechtigt;
    }

}
