package at.gv.brz.trainee.um.utility;

/**
 * Konstanten für die String der Navigation und für die MD5 Verschlüsselung.
 * 
 * @author duha
 *
 */
public final class Constants {
    
    private Constants() {
        super();
    }

    public static final String NAVIGATION_ON_SUCCESS = "success";

    public static final String NAVIGATION_ON_FAILURE = "failure";
    
    public static final  char[] HEX_ARRAY = "0123456789abcdef".toCharArray();
    
    public static final String PASSWORT_FALSCH_TITLE = "Falsches Passwort";
    
    public static final String PASSWORT_FALSCH_DETAIL = "Sie haben ein falsches Passwort eingegeben!";

}
