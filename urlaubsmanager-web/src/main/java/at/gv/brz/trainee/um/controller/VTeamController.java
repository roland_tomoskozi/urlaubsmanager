package at.gv.brz.trainee.um.controller;

import static at.gv.brz.trainee.um.utility.Constants.NAVIGATION_ON_SUCCESS;
import static at.gv.brz.trainee.um.utility.Constants.PASSWORT_FALSCH_DETAIL;
import static at.gv.brz.trainee.um.utility.Constants.PASSWORT_FALSCH_TITLE;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.User;
import at.gv.brz.trainee.um.model.UserVTeam;
import at.gv.brz.trainee.um.model.VTeam;
import at.gv.brz.trainee.um.service.impl.UserVTeamServiceJPA;
import at.gv.brz.trainee.um.service.impl.VTeamServiceJPA;
import at.gv.brz.trainee.um.utility.PasswortHasher;

/**
 * Diese Klasse enthält Methoden zur Verarbeitung von Anforderungen zum Beitreten des Teams, Passwort ueberpruefung und
 * Auflisten des aktuellen virtuellen Teams(VTam).
 * 
 * @author muradd
 *
 */
public class VTeamController implements Serializable {
    private static final long serialVersionUID = 1L;

    private VTeamServiceJPA vTeamService;

    private UserVTeamServiceJPA userVTeamService;

    private VTeam currentVTeam = new VTeam();

    private ListDataModel<VTeam> vteams;

    private User curreUser = new User();

    private String eingegebenesPasswort;

    /**
     * 
     * Diese Methode kontrolliert, ob der User das korrekte Team-Passwort eingegeben hat und fügt bei korrekter Eingabe
     * das Team hinzu.
     * 
     * @return String, um auf die nächste Navigationsseite zu verweisen
     * 
     */
    public String beitreten() {
        PasswortHasher ph = new PasswortHasher();

        try {
            eingegebenesPasswort = ph.genenerateMD5Passwort(eingegebenesPasswort);
            if (eingegebenesPasswort.equalsIgnoreCase(currentVTeam.getPasswort())) {
                UserVTeam uvteam = new UserVTeam();
                uvteam.setUser(curreUser);
                uvteam.setvTeam(currentVTeam);
                eingegebenesPasswort = "";
                userVTeamService.add(uvteam);
                currentVTeam = new VTeam();
                return NAVIGATION_ON_SUCCESS;
            } else {
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, PASSWORT_FALSCH_TITLE,
                                                             PASSWORT_FALSCH_DETAIL);
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                return "";
            }
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
            return "";
        } catch (ServiceException e) {
            e.printStackTrace();
            return "";
        }

    }

    /**
     * Diese Methode ist wichtig, für die Aufbereitung des derzeitigen Teams für den Dialog.
     */
    public void currentVTeamFuerDialog() {
        currentVTeam = vteams.getRowData();
    }

    /**
     * Weiterleitung zu MeineTeams Fehlermeldung wird zurückgesetzt.
     * 
     * @return cancel
     */
    public String zurueckZuMeineTeams() {
//    	passwortFalsch = false;
        return "cancel";
    }

    /**
     * Fehlermeldung wird zurückgesetzt, wenn man beim Dialog auf zurück drückt.
     */
    public void dialogZurueckButton() {
    }

    /*
     * **************** Getters & Setters ************************
     */

    public VTeam getCurrentVTeam() {
        return currentVTeam;
    }

    public void setCurrentVTeam(VTeam currentVTeam) {
        this.currentVTeam = currentVTeam;
    }

    public VTeamServiceJPA getVTeamService() {
        return vTeamService;
    }

    public void setVTeamService(VTeamServiceJPA vTeamService) {
        this.vTeamService = vTeamService;
    }

    public User getCurreUser() {
        return curreUser;
    }

    public void setCurreUser(User curreUser) {
        this.curreUser = curreUser;
    }

    public VTeamServiceJPA getvTeamService() {
        return vTeamService;
    }

    public void setvTeamService(VTeamServiceJPA vTeamService) {
        this.vTeamService = vTeamService;
    }

    public UserVTeamServiceJPA getUserVTeamService() {
        return userVTeamService;
    }

    public void setUserVTeamService(UserVTeamServiceJPA userVTeamService) {
        this.userVTeamService = userVTeamService;
    }

    public String getEingegebenesPasswort() {
        return eingegebenesPasswort;
    }

    public void setEingegebenesPasswort(String eingegebenesPasswort) {
        this.eingegebenesPasswort = eingegebenesPasswort;
    }

    public ListDataModel<VTeam> getVteams() throws ServiceException {
        this.vteams = new ListDataModel<>(vTeamService.getVTeamsUserNotJoined(curreUser));

        return vteams;
    }

    public void setVteams(ListDataModel<VTeam> vteams) {
        this.vteams = vteams;
    }
}
