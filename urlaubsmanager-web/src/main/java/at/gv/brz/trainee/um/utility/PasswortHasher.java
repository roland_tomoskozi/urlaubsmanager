package at.gv.brz.trainee.um.utility;

import static at.gv.brz.trainee.um.utility.Constants.HEX_ARRAY;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Methoden dieser Klasse sind für das verschlüsseln des Passworts zuständig.
 * 
 * @author schustefeli
 *
 */
public class PasswortHasher {

    /**
     * Ein String wird mit MD5 verschlüsselt.
     * 
     * @param passwort 
     * @return String, Passwort wird in MD5 verschlüsselt
     * @throws NoSuchAlgorithmException 
     * @author schustefeli && schrittn
     */
    public String genenerateMD5Passwort(String passwort) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.reset();
        byte[] hash = digest.digest(passwort.getBytes());
        return bytesToStringHex(hash);
    }

    /**
     * Hilfsmethode für die MD5 Verschlüsselung.
     * 
     * @param bytes
     * @return String
     * @author schustefeli && schrittn
     */
    private String bytesToStringHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int i = 0; i < bytes.length; i++) {
            int v = bytes[i] & 0xFF;
            hexChars[i * 2] = HEX_ARRAY[v >>> 4];
            hexChars[i * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

}
