package at.gv.brz.trainee.um.controller;

import static at.gv.brz.trainee.um.utility.Constants.NAVIGATION_ON_FAILURE;
import static at.gv.brz.trainee.um.utility.Constants.NAVIGATION_ON_SUCCESS;

import java.io.Serializable;

import javax.el.ELException;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.User;
import at.gv.brz.trainee.um.model.VTeam;
import at.gv.brz.trainee.um.service.impl.VTeamServiceJPA;

/**
 * Diese Klasse enthält Methoden zur Verarbeitung von virtuellen Teams eines Users.
 * 
 * @author muradd
 *
 */
public class MeineVTeamsController implements Serializable {
    private static final long serialVersionUID = 1L;

    private VTeamServiceJPA vTeamService;

    private ListDataModel<VTeam> vTeams;

    private User currentUser;

    private VTeam currentVTeam = new VTeam();

    private boolean berechtigt;

    /**
     * Zeigt alle VTeams in denen der User im Moment Mitglied ist.
     *
     * @return NAVIGATION_ON_SUCCESS Navigation zu allen VTeams
     * 
     **/
    public String showMeineVTeams() throws ServiceException {
        currentVTeam = vTeams.getRowData();
        return NAVIGATION_ON_SUCCESS;
    }

    public String showurlaubsantraege() {
        FacesContext ctx = FacesContext.getCurrentInstance();

        UrlaubAntraegeController uac = ctx.getApplication().evaluateExpressionGet(ctx, "#{urlaubAntraegeController}",
                                                                                  UrlaubAntraegeController.class);

        uac.setCurrentUser(currentUser);
        uac.setAbgelehnt(false);
        uac.setGenehmigt(false);
        try {
            currentVTeam = vTeams.getRowData();

            uac.setCurrentVTeam(currentVTeam);
        } catch (IllegalStateException | ELException | FacesException e) {
            e.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Weiterleitung zu allen VTeams mit Übergabe des aktuellen Users.
     * 
     * @return NAVIGATION_ON_SUCCESS Navigation zu allen VTeams
     */
    public String showAlleVTeams() {
        FacesContext ctx = FacesContext.getCurrentInstance();

        VTeamController vtc =
                ctx.getApplication().evaluateExpressionGet(ctx, "#{vTeamController}", VTeamController.class);
        vtc.setCurreUser(currentUser);

        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Zeige alle Urlaube + Weiterleitung.
     *
     * @return NAVIGATION_ON_SUCCESS
     * 
     **/
    public String showVTeamDetail() {
        currentVTeam = vTeams.getRowData();
        FacesContext ctx = FacesContext.getCurrentInstance();
        UrlaubController uc =
                ctx.getApplication().evaluateExpressionGet(ctx, "#{urlaubController}", UrlaubController.class);
        uc.setCurrentVTeam(currentVTeam);
        uc.setCurrentUser(currentUser);
        return NAVIGATION_ON_SUCCESS;
    }

    /*
     * **************** Getters & Setters ************************
     */

    public ListDataModel<VTeam> getvTeams() {
        try {
            this.vTeams = new ListDataModel<>(vTeamService.getVTeamsByUser(currentUser));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return this.vTeams;
    }

    public void setvTeams(ListDataModel<VTeam> vTeams) {
        this.vTeams = vTeams;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public VTeamServiceJPA getvTeamService() {
        return vTeamService;
    }

    public void setvTeamService(VTeamServiceJPA vTeamService) {
        this.vTeamService = vTeamService;
    }

    public VTeam getCurrentVTeam() {
        return currentVTeam;
    }

    public void setCurrentVTeam(VTeam currentVTeam) {
        this.currentVTeam = currentVTeam;
    }

    public boolean isBerechtigt() {
        return berechtigt;
    }

    public void setBerechtigt(boolean berechtigt) {
        this.berechtigt = berechtigt;
    }
}