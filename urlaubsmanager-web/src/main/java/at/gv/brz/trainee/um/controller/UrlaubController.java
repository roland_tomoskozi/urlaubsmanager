package at.gv.brz.trainee.um.controller;

import static at.gv.brz.trainee.um.utility.Constants.NAVIGATION_ON_FAILURE;
import static at.gv.brz.trainee.um.utility.Constants.NAVIGATION_ON_SUCCESS;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.ListDataModel;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.Urlaub;
import at.gv.brz.trainee.um.model.User;
import at.gv.brz.trainee.um.model.VTeam;
import at.gv.brz.trainee.um.model.enums.Status;
import at.gv.brz.trainee.um.service.UrlaubService;
import at.gv.brz.trainee.um.service.impl.UrlaubServiceJPA;

/**
 * Diese Klasse enthält Methoden zur Verarbeitung von Anforderungen zum Speichern, Aktualisieren, Löschen und
 * Auflisten von Urlauben.
 * 
 * @author muradd, Kommentaruebersetzung: janua
 *
 */
public class UrlaubController implements Serializable {
    private static final long serialVersionUID = 1L;

    private UrlaubServiceJPA urlaubService;

    private Urlaub currentUrlaub = new Urlaub();

    private VTeam currentVTeam = new VTeam();

    private User currentUser;

    private ListDataModel<Urlaub> urlaube;

    /**
     * Methode: ruft add() Methode von UrlaubsService auf. Verknuepft aktuellen Urlaub mit aktuellem Nutzer, fuegt den
     * aktuellen Urlaub hinzu.
     * 
     * @return NAVIGATION_ON_SUCCESS wenn add() funktioniert, sonst NAVIGATION_ON_FAILURE
     **/
    public String save() {
        currentUrlaub.setUser(currentUser);
        try {
            urlaubService.add(currentUrlaub);
        } catch (ServiceException se) {
            se.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Methode: ruft upate() Methode von UrlaubService auf. Aktualisiert den aktuellen Urlaub.
     *
     * @return NAVIGATION_ON_SUCCESS wenn update() funktioniert, sonst NAVIGATION_ON_FAILURE
     **/
    public String update() {
        try {
            urlaubService.update(currentUrlaub);
        } catch (ServiceException se) {
            se.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }

        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Methode: verknüpft den aktuellen Urlaub mit der korrekten Zeile.
     *
     * @return NAVIGATION_ON_SUCCESS wenn showEdit() funktioniert, sonst NAVIGATION_ON_FAILURE
     **/
    public String showEdit() {
        currentUrlaub = urlaube.getRowData();
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Methode: ruft Methode delete() von Urlaubsservice auf. Diese setzt Variable deleted von Urlaub auf true damit
     * Urlaub nicht komplett aus der Datenbank geloescht wird. Verknuepft den aktuellen Urlaub mit der korrekten Zeile.
     * Geloeschte Urlaube werden in der Urlaubsliste nicht mehr angezeigt.
     * 
     * @return NAVIGATION_ON_SUCCESS wenn delete() funktioniert, sonst NAVIGATION_ON_FAILURE
     **/
    public String delete() {
        try {
            urlaubService.delete(currentUrlaub.getId());
        } catch (ServiceException se) {
            se.printStackTrace();
            return NAVIGATION_ON_FAILURE;
        }

        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Methode: setzt aktuellen Urlaub zurueck.
     *
     * @return NAVIGATION_ON_SUCCESS
     **/
    public String showInsert() {
        currentUrlaub = new Urlaub();
        return NAVIGATION_ON_SUCCESS;
    }

    /**
     * Der Urlaub wird für das Pop-Up Fenster gesetzt, um in der Delete Methode verwendet zu werden.
     */
    public void urlaubForDialog() {
        currentUrlaub = urlaube.getRowData();
        setCurrentUrlaub(currentUrlaub);
    }

    /**
     * Überprüft, ob sich die Daten in einer Liste überscheiden. Es wird jedes Datum einer Reihe einzeln überprüft
     * und mit den Daten der Liste verglichen. <br>
     * 
     * @return true, wenn das Datum überschneidet ein anderes Datum.
     * @throws ServiceException
     *             falls etwas mit der Service nicht stimmt.
     * @author gaalcs, Felix, Daniel
     */
    public boolean isUrlaubUeberschneidet() throws ServiceException {
        Urlaub urlaub = urlaube.getRowData();
        long urlaubStartDatum = urlaub.getStartDatum().getTime();
        long urlaubEndDatum = urlaub.getEndDatum().getTime();
        List<Urlaub> hilfliste = getUrlaubeSortiertByDate();
        for (int i = 0; i < hilfliste.size(); i++) {
            if (urlaub.getId() == hilfliste.get(i).getId()) {
                continue;
            }
            long listStartDatum = hilfliste.get(i).getStartDatum().getTime();
            long listEndDatum = hilfliste.get(i).getEndDatum().getTime();
            if ((urlaubStartDatum >= listStartDatum && urlaubStartDatum <= listEndDatum)
                || (urlaubEndDatum >= listStartDatum && urlaubEndDatum <= listEndDatum)
                || (urlaubStartDatum < listStartDatum && urlaubEndDatum > listEndDatum)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Vergleichet den aktuellen User von DataTable Row mit dem currentUser.
     * 
     * @return true, wenn der User gleich ist mit dem currentUser.
     * @author gaalcs
     */
    public boolean isUserCurrentUser() {
        User user = urlaube.getRowData().getUser();
        if (user.getId() == currentUser.getId()) {
            return true;
        }
        return false;
    }

    /**
     * Vergleicht User von DataTable Row mit currentUser und prüft, ob Status von Urlaub Offen ist.
     * 
     * @return true, wenn User gleich dem currentUser ist und der Urlaub Status OFFEN hat
     * @author konrathi
     */
    public boolean isUserCurrentUserWithStatusOffen() {
        Urlaub urlaub = urlaube.getRowData();

        if (urlaub.getUser().getId() == currentUser.getId() && urlaub.getStatus().equals(Status.OFFEN)) {
            return true;
        }
        return false;
    }

    /**
     * Holt eine Urlaubliste nach VTeam Id von der Datenbank und sortiert die Liste nach Start Datum.
     * 
     * @return Eine Liste von Urlaube nach Star Datum sortiert.
     * @throws ServiceException
     *             falls etwas mit der Service nicht stimmt.
     * @author gaalcs
     */
    public List<Urlaub> getUrlaubeSortiertByDate() throws ServiceException {
        List<Urlaub> urlaublist = urlaubService.getUrlaubListByVTeam(currentVTeam.getId());
        return urlaublist.stream().sorted((u1, u2) -> u1.getStartDatum().compareTo(u2.getStartDatum()))
                         .collect(Collectors.toList());
    }

    /*
     * ***************** Constructor **************************
     */

    public UrlaubController() {
        super();
    }

    /*
     * *****************Getters & Setters***********************
     */

    public Urlaub getCurrentUrlaub() {
        return currentUrlaub;
    }

    public void setCurrentUrlaub(Urlaub currentUrlaub) {
        this.currentUrlaub = currentUrlaub;
    }

    public ListDataModel<Urlaub> getUrlaube() throws ServiceException {
        this.urlaube = new ListDataModel<>(getUrlaubeSortiertByDate());
        return this.urlaube;
    }

    public void setUrlaube(ListDataModel<Urlaub> urlaube) {
        this.urlaube = urlaube;
    }

    public UrlaubService getUrlaubService() {
        return urlaubService;
    }

    public void setUrlaubService(UrlaubServiceJPA urlaubService) {
        this.urlaubService = urlaubService;
    }

    public VTeam getCurrentVTeam() {
        return currentVTeam;
    }

    public void setCurrentVTeam(VTeam currentVTeam) {
        this.currentVTeam = currentVTeam;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public List<Status> getStatus() {
        List<Status> status = new ArrayList<>();
        status.add(Status.OFFEN);
        status.add(Status.GENEHMIGT);
        status.add(Status.ABGELEHNT);
        return status;
    }
}