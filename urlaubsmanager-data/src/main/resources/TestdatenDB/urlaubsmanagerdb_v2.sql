-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 21. Nov 2019 um 11:33
-- Server-Version: 10.4.8-MariaDB
-- PHP-Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `urlaubsmanagerdb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `urlaub`
--

CREATE TABLE `urlaub` (
  `id` int(11) NOT NULL,
  `start_datum` date NOT NULL,
  `end_datum` date NOT NULL,
  `status` varchar(255) NOT NULL,
  `kommentar` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `fk_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `urlaub`
--

INSERT INTO `urlaub` (`id`, `start_datum`, `end_datum`, `status`, `kommentar`, `deleted`, `fk_user`) VALUES
(1, '2019-12-15', '2019-12-20', 'OFFEN', 'Brauche dringend', 0, 2),
(2, '2019-12-15', '2019-12-20', 'OFFEN', 'Ich auch', 0, 9),
(3, '2020-01-12', '2020-01-15', 'GENEHMIGT', 'Kommentar', 0, 5),
(4, '2020-01-26', '2020-01-30', 'ABGELEHNT', 'Schade', 0, 1),
(5, '2019-12-22', '2019-12-23', 'OFFEN', 'offen', 0, 3),
(6, '2019-12-02', '2019-12-05', 'OFFEN', 'offen', 0, 3),
(7, '2019-12-22', '2019-12-27', 'ABGELEHNT', 'Nordkorea', 0, 1),
(8, '2019-12-28', '2020-01-06', 'OFFEN', 'Türkei', 0, 5),
(9, '2020-06-02', '2020-06-15', 'GENEHMIGT', 'Italien', 0, 2),
(10, '2020-05-26', '2020-05-29', 'ABGELEHNT', 'Schweiz', 0, 3),
(11, '2020-03-15', '2020-03-27', 'OFFEN', 'Irland', 1, 4),
(12, '2020-07-09', '2020-07-22', 'OFFEN', 'Frankreich', 0, 6),
(13, '2020-07-16', '2020-07-30', 'GENEHMIGT', 'USA', 0, 7),
(14, '2020-07-27', '2020-08-12', 'OFFEN', 'England', 0, 8),
(15, '2020-07-27', '2020-08-12', 'ABGELEHNT', 'Nordpol', 0, 9),
(16, '2020-08-04', '2020-08-25', 'OFFEN', 'Kroatien', 0, 10),
(17, '2019-12-24', '2019-01-04', 'ABGELEHNT', 'Spanien', 0, 11),
(18, '2019-12-20', '2020-01-06', 'OFFEN', 'Iran', 0, 14),
(19, '2020-02-02', '2020-02-21', 'GENEHMIGT', 'China', 0, 16),
(20, '2020-05-20', '2020-05-29', 'ABGELEHNT', 'Schweden', 0, 17),
(21, '2020-03-05', '2020-03-16', 'OFFEN', 'Deutschland', 1, 22),
(22, '2020-07-05', '2020-07-18', 'OFFEN', 'Russland', 0, 19),
(23, '2020-07-15', '2020-07-30', 'GENEHMIGT', 'Indien', 1, 13),
(24, '2020-07-25', '2020-08-12', 'OFFEN', 'Peru', 0, 15),
(25, '2020-07-22', '2020-08-10', 'ABGELEHNT', 'Südpol', 0, 25),
(26, '2020-08-01', '2020-08-21', 'OFFEN', 'Japan', 0, 24);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `vorname` varchar(255) NOT NULL,
  `nachname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `passwort` varchar(255) NOT NULL,
  `rolle` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `vorname`, `nachname`, `email`, `passwort`, `rolle`, `deleted`) VALUES
(1, 'Felix', 'Schuster', 'Felix.Schuster@brz.gv.at', 'Felix123', 'MA', 0),
(2, 'Csaba', 'Gaal', 'csaba.gaal@brz.gv.at', 'Csaba123', 'MA', 0),
(3, 'Isabella', 'Konrath', 'izabella.konrath@brz.gv.at', 'Isabella123', 'MA', 0),
(4, 'Duha', 'Murad', 'duha.murad@brz.gv.at', 'Duha123', 'MA', 0),
(5, 'Amelie', 'Janu', 'amelie.janu@brz.gv.at', 'Amelie123', 'MA', 0),
(6, 'Emese', 'Varga', 'emese.varga@brz.gv.at', 'Emese123', 'MA', 0),
(7, 'Ashkan', 'Pishbin', 'ashkan.pishbin@brz.gv.at', 'Ashkan123', 'MA', 0),
(8, 'Roland', 'Tömösközi', 'roland.toemoeskoezi@brz.gv.at', 'Roland123', 'MA', 0),
(9, 'Daniel', 'Datzinger', 'daniel.datzinger@brz.gv.at', 'Daniel123', 'MA', 0),
(10, 'Nicole', 'Schrittwieser', 'nicole.schrittwieser@brz.gv.at', 'Nicole123', 'MA', 0),
(11, 'Max', 'Meier', 'Max.Meier@brz.gv.at', 'Max123', 'TL', 0),
(12, 'Rudi', 'Rüssel', 'Rudi.Rüssel@brz.gv.at', 'Rudi123', 'TL', 0),
(13, 'Anna', 'Steiner', 'Anna.Steiner@brz.gv.at', 'Anna123', 'TL', 0),
(14, 'Franz', 'Richter', 'Franz.Richter@brz.gv.at', 'Franz123', 'TL', 0),
(15, 'Eva', 'Huber', 'Eva.Huber@brz.gv.at', 'Eva123', 'TL', 0),
(16, 'Herbert', 'Fuchs', 'Herbert.Fuchs@brz.gv.at', 'Herbert123', 'TL', 0),
(17, 'Michael', 'Müller', 'Michael.Müller@brz.gv.at', 'Michael123', 'TL', 0),
(18, 'Reinhard', 'Storch', 'Reinhard.Storch@brz.gv.at', 'Reinhard123', 'PL', 0),
(19, 'Harry', 'Potter', 'Harry.Potter@brz.gv.at', 'Harry123', 'PL', 0),
(20, 'Bilbo', 'Beutlin', 'Bilbo.Beutlin@brz.gv.at', 'Bilbo123', 'ADMIN', 0),
(21, 'Brandon', 'Stark', 'Brandon.Stark@brz.gv.at', 'Brandon123', 'MA', 0),
(22, 'James', 'Bond', 'James.Bond@brz.gv.at', 'James123', 'MA', 0),
(23, 'John', 'Rambo', 'John.Rambo@brz.gv.at', 'John123', 'MA', 1),
(24, 'Bill', 'Gates', 'Bill.Gates@brz.gv.at', 'Bill123', 'PM', 0),
(25, 'Lara', 'Croft', 'Lara.Croft@brz.gv.at', 'Lara123', 'PM', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_vteam`
--

CREATE TABLE `user_vteam` (
  `id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `fk_user` int(11) NOT NULL,
  `fk_vteam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `user_vteam`
--

INSERT INTO `user_vteam` (`id`, `deleted`, `fk_user`, `fk_vteam`) VALUES
(1, 0, 5, 1),
(2, 0, 5, 5),
(3, 0, 7, 1),
(4, 0, 7, 3),
(5, 0, 2, 1),
(6, 0, 2, 3),
(7, 0, 9, 1),
(8, 0, 9, 7),
(9, 0, 4, 1),
(10, 0, 4, 4),
(11, 0, 6, 1),
(12, 0, 6, 5),
(13, 0, 1, 1),
(14, 0, 1, 2),
(15, 0, 3, 1),
(16, 0, 3, 6),
(17, 0, 10, 1),
(18, 0, 10, 3),
(19, 0, 8, 1),
(20, 0, 8, 2),
(21, 0, 11, 1),
(22, 0, 12, 2),
(23, 0, 13, 3),
(24, 0, 14, 4),
(25, 0, 15, 5),
(26, 0, 16, 6),
(27, 0, 17, 7),
(28, 0, 18, 2),
(29, 0, 19, 3),
(30, 0, 22, 4),
(31, 0, 21, 5),
(32, 0, 22, 6),
(33, 1, 23, 7),
(34, 0, 24, 2),
(35, 0, 25, 3),
(36, 0, 21, 4),
(37, 0, 24, 5),
(38, 1, 23, 6),
(39, 0, 18, 7),
(40, 0, 25, 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vteam`
--

CREATE TABLE `vteam` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `passwort` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `vteam`
--

INSERT INTO `vteam` (`id`, `name`, `passwort`, `deleted`) VALUES
(1, 'Trainee', 'Trainee123', 0),
(2, 'Business Logic 1', 'Logic123', 0),
(3, 'Business Logic2', 'Logic123', 0),
(4, 'Business Logic 3', 'Logic123', 0),
(5, 'Business Logic4', 'Logic123', 0),
(6, 'Frontend Web1', 'Frontend123', 0),
(7, 'Frontend Web2', 'Frontend123', 0);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `urlaub`
--
ALTER TABLE `urlaub`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_urlaub` (`fk_user`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `user_vteam`
--
ALTER TABLE `user_vteam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user` (`fk_user`),
  ADD KEY `fk_vteam` (`fk_vteam`);

--
-- Indizes für die Tabelle `vteam`
--
ALTER TABLE `vteam`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `urlaub`
--
ALTER TABLE `urlaub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT für Tabelle `user_vteam`
--
ALTER TABLE `user_vteam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT für Tabelle `vteam`
--
ALTER TABLE `vteam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `urlaub`
--
ALTER TABLE `urlaub`
  ADD CONSTRAINT `fk_user_urlaub` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `user_vteam`
--
ALTER TABLE `user_vteam`
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vteam` FOREIGN KEY (`fk_vteam`) REFERENCES `vteam` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
