package at.gv.brz.trainee.um.service.impl;

import static at.gv.brz.trainee.um.utility.Constants.PERSISTANCE_UNIT_NAME;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Service;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.Urlaub;
import at.gv.brz.trainee.um.model.User;
import at.gv.brz.trainee.um.model.VTeam;
import at.gv.brz.trainee.um.model.enums.Status;
import at.gv.brz.trainee.um.service.UrlaubService;

/**
 * Serviceklasse für Urlaub.
 * 
 * @author schrittn
 *
 */
@Service
public class UrlaubServiceJPA implements UrlaubService {

    private static EntityManagerFactory f = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT_NAME);

    /**
     * Adden von einem Urlaub.
     * 
     * @throws ServiceException
     */
    @Override
    public void add(Urlaub urlaub) throws ServiceException {

        if (urlaub == null || validateDatum(urlaub.getStartDatum(), urlaub.getEndDatum())) {
            throw new ServiceException("Der Urlaub ist Null");
        }
        EntityManager em = f.createEntityManager();
        EntityTransaction tranc = null;

        try {
            tranc = em.getTransaction();
            tranc.begin();
            urlaub.setStatus(Status.OFFEN);

            em.persist(urlaub);
            em.close();
            tranc.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            tranc.rollback();
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        } finally {
            em.close();

        }
    }

    /**
     * UrlaubListe bekommen.
     * 
     * @return Urlaubsliste
     * @throws ServiceException
     */
    @Override
    public List<Urlaub> getUrlaubList() throws ServiceException {

        EntityManager em = f.createEntityManager();
        EntityTransaction tranc = null;

        try {
            tranc = em.getTransaction();
            tranc.begin();

            List<Urlaub> urlaube =
                    em.createQuery("SELECT u from Urlaub u WHERE u.deleted = '" + false + "'", Urlaub.class)
                      .getResultList();

            tranc.commit();
            return urlaube;

        } catch (IllegalStateException | IllegalArgumentException | PersistenceException e) {
            tranc.rollback();
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        } finally {
            em.close();
        }
    }

    /**
     * UrlaubListe zu einem VTeam bekommen.
     * 
     * @param id
     * @return Urlaubsliste
     * @throws ServiceException
     */
    public List<Urlaub> getUrlaubListByVTeam(int id) throws ServiceException {

        if (id < 0) {
            throw new ServiceException("Urlaubsid ist negativ");
        }

        UserServiceJPA u = new UserServiceJPA();
        List<User> user = u.getUserByVTeam(id);
        List<Urlaub> urlaube = new ArrayList<>();
        for (User user2 : user) {

            List<Urlaub> ur = user2.getUrlaubListe();
            if (ur != null) {
                for (Urlaub urlaub : ur) {
                    if (!urlaub.isDeleted()) {
                        urlaube.add(urlaub);
                    }
                }
            }
        }

        return urlaube;

    }

    /**
     * Gibt die Urlaube die den Status offen haben.
     * 
     * @param vteam
     * @param currentUser
     * @return List der Urlaube
     * @throws ServiceException
     */
    public List<Urlaub> getUrlaubListOffen(VTeam vteam, User currentUser) throws ServiceException {

        if (vteam == null || currentUser == null) {
            throw new ServiceException("Der Vteam ist Null");
        }

        UserServiceJPA u = new UserServiceJPA();
        List<User> user = u.getUserByVTeam(vteam.getId());
        List<Urlaub> urlaube = new ArrayList<>();
        for (User user2 : user) {
            if (user2.getId() != currentUser.getId()) {
                List<Urlaub> ur = user2.getUrlaubListe();
                if (ur != null) {
                    for (Urlaub urlaub1 : ur) {
                        if (urlaub1.getStatus() == Status.OFFEN) {
                            urlaube.add(urlaub1);
                        }
                    }
                }
            }
        }

        return urlaube;

    }

    /**
     * Setzte Status abgelehnt.
     * 
     * @param urlaub
     * @throws ServiceException
     */
    public void setUrlaubabgelehnt(Urlaub urlaub) throws ServiceException {

        EntityManager em = f.createEntityManager();
        EntityTransaction tranc = null;
        if (urlaub == null) {
            throw new ServiceException("Der Urlaub ist Null");
        }

        try {
            tranc = em.getTransaction();
            tranc.begin();
            urlaub.setStatus(Status.ABGELEHNT);
            urlaub = em.merge(urlaub);
            em.persist(urlaub);
            tranc.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            tranc.rollback();
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        } finally {
            em.close();
        }
    }

    /**
     * Setzte genehmigt.
     * 
     * @param urlaub
     * @throws ServiceException
     */
    public void setUrlaubgnehmigt(Urlaub urlaub) throws ServiceException {
        EntityManager em = f.createEntityManager();
        EntityTransaction tranc = null;
        if (urlaub == null) {
            throw new ServiceException("Der Urlaub ist Null");
        }

        try {
            tranc = em.getTransaction();
            tranc.begin();
            urlaub.setStatus(Status.GENEHMIGT);
            urlaub = em.merge(urlaub);
            em.persist(urlaub);
            tranc.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            tranc.rollback();
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        } finally {
            em.close();
        }
    }

    /**
     * Urlaub updaten.
     * 
     * @throws ServiceException
     */
    @Override
    public void update(Urlaub urlaub) throws ServiceException {
        EntityManager em = f.createEntityManager();
        EntityTransaction tranc = em.getTransaction();
        if (urlaub == null || validateDatum(urlaub.getStartDatum(), urlaub.getEndDatum())) {
            throw new ServiceException("Der Urlaub ist Null");
        }
        try {
            tranc.begin();
            urlaub = em.merge(urlaub);
            em.persist(urlaub);
            tranc.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            tranc.rollback();
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        } finally {
            em.close();
        }
    }

    /**
     * gibt einen Urlaub zurück.
     * 
     * @return Urlaub
     * @throws ServiceException
     */
    @Override
    public Optional<Urlaub> get(int id) throws ServiceException {
        EntityManager em = f.createEntityManager();
        EntityTransaction tranc = null;

        if (id < 0) {
            throw new ServiceException("Urlaubsid ist negativ");
        }
        try {
            tranc = em.getTransaction();
            tranc.begin();

            tranc.begin();
            List<Urlaub> urlaube =
                    em.createQuery("SELECT u from Urlaub u WHERE u.id = " + id + " AND u.delete = false", Urlaub.class)
                      .getResultList();

            tranc.commit();
            return urlaube.stream().findFirst();
        } catch (IllegalStateException | IllegalArgumentException | NullPointerException | PersistenceException e) {

            tranc.rollback();
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        } finally {
            em.close();
        }
    }

    /**
     * Setzt einen urlaub auf deleted.
     * 
     * @throws ServiceException
     */
    @Override
    public void delete(int id) throws ServiceException {
        EntityManager em = f.createEntityManager();
        EntityTransaction tranc = null;
        if (id < 0) {
            throw new ServiceException("Urlaubsid ist negativ");
        }

        try {
            tranc = em.getTransaction();
            tranc.begin();

            Urlaub u = em.find(Urlaub.class, id);
            u.setDeleted(true);
            u = em.merge(u);
            em.persist(u);
            tranc.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {

            tranc.rollback();
            e.printStackTrace();
            throw new ServiceException(e.getMessage());

        } finally {
            em.close();
        }
    }

    /**
     * Validator für den die Urlaubsklasse.
     * 
     * @param startDatum
     * @param endDatum
     * @return boolean
     */
    public boolean validateDatum(Date startDatum, Date endDatum) {
        Date now = new Date();
        if (startDatum.before(now) || endDatum.before(startDatum)) {
            return true;
        }
        return false;
    }
}
