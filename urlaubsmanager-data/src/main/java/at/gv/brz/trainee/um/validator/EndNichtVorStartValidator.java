package at.gv.brz.trainee.um.validator;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.sun.faces.util.MessageFactory;

/**
 * Ueberprueft, ob der Endate des Urlaubs vor der Startdate steht. Wenn dieser Fall ist, wirft er eine Nachricht.
 * 
 * @author toemoes
 *
 */
public class EndNichtVorStartValidator implements Validator {

    public static final String END_DATE_BEFORE_START_DATE_MESSAGE =
            "at.gv.brz.trainee.um.validator.EndVorStartValidator.END_DATE_BEFORE_START_DATE_MESSAGE";

    /**
     * Ueberpruft, ob der Endate des Urlaubs vor der Startdate steht. Wenn dieser Fall ist, wirft er eine Nachricht.
     */
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        if (value == null) {
            return;
        }
        UIInput startDatumEingabe = (UIInput) component.getAttributes().get("startDatumComponent");

        if (startDatumEingabe.getValue() == null && !startDatumEingabe.isValid()) {
            return;
        }

        Date startDatum = (Date) startDatumEingabe.getValue();
        Date endDatum = (Date) value;

        if (!doValidate(startDatum, endDatum)) {
            FacesMessage message =
                    MessageFactory.getMessage(END_DATE_BEFORE_START_DATE_MESSAGE, FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }

    /**
     * Hilfmethode, damit man den Validation leichter testen kann.
     * 
     * @param startDatum 
     * @param endDatum 
     * 
     *            Wenn kein Value fuer enddatum gesetzt Wenn kein Value fuer startdatum gesetzt enddatum keine instance
     *            of Date validiert ob enddatum vor startdatum
     * 
     * @return true oder false
     */
    public boolean doValidate(Date startDatum, Date endDatum) {

        if (endDatum == null) {
            return false;
        }

        if (startDatum == null) {
            return false;
        }

        if (!(endDatum instanceof Date)) {
            return false;
        }

        if (!(startDatum instanceof Date)) {
            return false;
        }

        if (endDatum.before(startDatum)) {
            return false;
        }

        return true;
    }
}