package at.gv.brz.trainee.um.service;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.UserVTeam;

/**
 * Interface für UserVTeamJPA.
 * 
 * @author datzing
 */
public interface UserVTeamService {

    void add(UserVTeam userVTeam) throws ServiceException;

    void delete(int id) throws ServiceException;

}
