package at.gv.brz.trainee.um.exception;

/**
 * Die Serviceexception Klasse gibt die Fehlermeldungen in einem schoeneren Form zurueck.
 * Vorteil: Man kann spaeter eigene Exceptions definieren.
 * 
 * @author pishbin
 */
public class ServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
