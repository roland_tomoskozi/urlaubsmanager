package at.gv.brz.trainee.um.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import at.gv.brz.trainee.um.model.enums.Rolle;

/**
 * Entity Klasse f�r einen User.
 * 
 * @author schustefeli
 *
 */
@Entity
@Table(name = "user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "vorname", nullable = false, length = 255)
    private String vorname;

    @Column(name = "nachname", nullable = false, length = 255)
    private String nachname;

    @Column(name = "email", nullable = false, length = 255)
    private String email;

    @Column(name = "passwort", nullable = false, length = 255)
    private String passwort;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @Column(name = "rolle", nullable = false, length = 255)
    @Enumerated(EnumType.STRING)
    private Rolle rolle;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<UserVTeam> userVTeamliste;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Urlaub> urlaubListe;

    /*
     * Getter && Setter
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswort() {

        return passwort;
    }

    public void setPasswort(String passwort) {

        this.passwort = passwort;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Rolle getRolle() {
        return rolle;
    }

    public void setRolle(Rolle rolle) {
        this.rolle = rolle;
    }

    public List<UserVTeam> getUserVTeamliste() {
        return userVTeamliste;
    }

    public void setUserVTeamliste(List<UserVTeam> userVTeamliste) {
        this.userVTeamliste = userVTeamliste;
    }

    public List<Urlaub> getUrlaubListe() {
        return urlaubListe;
    }

    public void setUrlaubListe(List<Urlaub> urlaubListe) {
        this.urlaubListe = urlaubListe;
    }

    public Rolle[] getRolleListe() {
        return Rolle.values();
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", vorname=" + vorname + ", nachname=" + nachname + ", email=" + email + ", passwort="
               + passwort + ", deleted=" + deleted + ", rolle=" + rolle + ", userVTeamliste=" + userVTeamliste
               + "]";
    }
}
