package at.gv.brz.trainee.um.service;

import java.util.List;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.User;

/**
 * Alle Methode für UserService.
 * 
 * @author gaalcs
 *
 */
public interface UserService {

    void addUser(User user) throws ServiceException;

    void updateUser(User user) throws ServiceException;

    void deleteUser(User user) throws ServiceException;

    List<User> getAllUser() throws ServiceException;

    User getUserByEmailAndPass(String email, String pass) throws ServiceException;

    User getUserById(int id) throws ServiceException;
  
    List<User> getUserByVTeam(int id) throws ServiceException;

}
