package at.gv.brz.trainee.um.service.impl;

import static at.gv.brz.trainee.um.utility.Constants.PERSISTANCE_UNIT_NAME;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.User;
import at.gv.brz.trainee.um.service.UserService;

/**
 * Implementierung von der UserService Interface.
 * 
 * @author gaalcs
 *
 */
@Service
public class UserServiceJPA implements UserService, Serializable {

    private static final long serialVersionUID = 1L;

    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT_NAME);

    private static final String GET_ALL_USER = "SELECT us FROM User us WHERE us.deleted = false";

    /**
     * Ein neu user wird in der Datenbank erstellt.
     * 
     * @param user
     * @throws ServiceException
     */
    @Override
    public void addUser(User user) throws ServiceException {
        EntityManager em = factory.createEntityManager();
        try {
            em = factory.createEntityManager();
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.persist(user);
            transaction.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            em.close();

        }
    }

    /**
     * Der user wird in der Datenbank aktualisiert.
     * 
     * @param user
     * @throws ServiceException
     */
    @Override
    public void updateUser(User user) throws ServiceException {
        EntityManager em = factory.createEntityManager();
        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            user = em.merge(user);
            em.persist(user);
            transaction.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            em.close();

        }
    }

    /**
     * Der user wird von der Datenbank gelöscht. (User auf deleted gesetzt.)
     * 
     * @param user
     * @throws ServiceException
     */
    @Override
    public void deleteUser(User user) throws ServiceException {
        EntityManager em = factory.createEntityManager();
        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            User userFromDB = em.find(User.class, user.getId());
            userFromDB.setDeleted(true);
            userFromDB = em.merge(userFromDB);
            em.persist(userFromDB);
            transaction.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            em.close();

        }
    }

    /**
     * Liefert alle User von der Datenbank.
     * 
     * @return Eine Liste mit allen User.
     * @throws ServiceException
     */
    @Override
    public List<User> getAllUser() throws ServiceException {
        List<User> users = null;
        EntityManager em = factory.createEntityManager();
        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            TypedQuery<User> query = em.createQuery(GET_ALL_USER, User.class);
            users = query.getResultList();
            transaction.commit();
        } catch (IllegalStateException | IllegalArgumentException | NullPointerException | PersistenceException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            em.close();

        }
        return users;
    }

    /**
     * Liefert ein User, wer die aingegebene email und passwort hat.
     * 
     * @param email
     *            Die email adresse vom User.
     * @param pass
     *            - Passwort von dem User.
     * @return - Liefert null zurück, wenn kein User gibt.
     * @throws ServiceException
     */
    @Override
    public User getUserByEmailAndPass(String email, String pass) throws ServiceException {
        EntityManager em = factory.createEntityManager();
        User userFind = null;
        List<User> users;
        if (email == null && pass == null) {
            throw new ServiceException("Email or pass is not existiert");
        }
        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            TypedQuery<User> query = em.createQuery("SELECT us FROM User us WHERE us.deleted = false AND us.email = '"
                                                    + email + "' AND us.passwort = '" + pass + "'", User.class);
            users = query.getResultList();
            if (users.size() == 0) {
                return null;
            } else {
                userFind = users.get(0);
            }
            transaction.commit();
        } catch (IllegalStateException | IllegalArgumentException | NullPointerException | PersistenceException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            em.close();

        }
        return userFind;
    }

    /**
     * Liefert ein User nach UserId zurück.
     * 
     * @param id
     *            - user Id
     * @return - Liefert ein User zurück
     * @throws ServiceException
     */
    @Override
    public User getUserById(int id) throws ServiceException {
        EntityManager em = factory.createEntityManager();
        User user = null;
        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            user = em.find(User.class, id);
            if (user.isDeleted()) {
                user = null;
            }
            transaction.commit();
        } catch (IllegalStateException | IllegalArgumentException | RollbackException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            em.close();

        }
        return user;
    }

    @Override
    public List<User> getUserByVTeam(int id) throws ServiceException {
        EntityManager em = factory.createEntityManager();
        List<User> users = null;
        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            TypedQuery<User> query =
                    em.createQuery("SELECT u FROM User u JOIN UserVTeam uvt ON u.id = uvt.user "
                                   + "JOIN VTeam vt ON uvt.vTeam = vt.id WHERE vt.id = '" + id + "'", User.class);
            users = query.getResultList();
            transaction.commit();
        } catch (IllegalStateException | IllegalArgumentException | PersistenceException e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            em.close();

        }
        return users;
    }

}
