package at.gv.brz.trainee.um.service.impl;

import static at.gv.brz.trainee.um.utility.Constants.PERSISTANCE_UNIT_NAME;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Service;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.User;
import at.gv.brz.trainee.um.model.VTeam;
import at.gv.brz.trainee.um.service.VTeamService;

/**
 * Serviceklasse für VTeam.
 * 
 * @author nicole
 *
 */
@Service
public class VTeamServiceJPA implements VTeamService, Serializable {
    private static final long serialVersionUID = 1L;

    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT_NAME);

    @Override
    public void addVTeam(VTeam vteam) throws ServiceException {

        EntityManager em = factory.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            if (vteam != null) {
                em.persist(vteam);
            } else {
                throw new ServiceException("Null User!");
            }

            transaction.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            transaction.rollback();
            e.printStackTrace();

            throw new ServiceException("Fehler beim hinzufügen der Entity in die Datenbank!", e);
        } finally {
            em.close();
        }

    }

    @Override
    public void updateVTeam(VTeam vteam) throws ServiceException {

        EntityManager em = factory.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();

            if (vteam != null) {

                vteam = em.merge(vteam);

                em.persist(vteam);
            } else {
                throw new ServiceException("Null User!");
            }

            transaction.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            transaction.rollback();
            e.printStackTrace();

            throw new ServiceException("Fehler beim aktualisieren der Entity in die Datenbank!", e);
        } finally {
            em.close();
        }

    }

    @Override
    public void deleteVTeam(Integer id) throws ServiceException {

        EntityManager em = factory.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        if (id < 0) {
            throw new ServiceException("VTeam Id ist negativ!");
        }

        try {
            transaction.begin();

            VTeam vteam = em.find(VTeam.class, id);
            vteam.setDeleted(true);
            vteam = em.merge(vteam);
            em.persist(vteam);

            transaction.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            transaction.rollback();
            e.printStackTrace();

            throw new ServiceException("Fehler beim löschen der Entity!", e);
        } finally {
            em.close();
        }

    }

    @Override
    public List<VTeam> getAllVTeams() throws ServiceException {

        EntityManager em = factory.createEntityManager();
        List<VTeam> vTeamList = new ArrayList<>();

        try {
            vTeamList = em.createQuery("SELECT vt FROM VTeam vt WHERE vt.deleted = '" + false + "'", VTeam.class)
                          .getResultList();

            return vTeamList;

        } catch (IllegalStateException | IllegalArgumentException | PersistenceException e) {
            e.printStackTrace();

            throw new ServiceException("Fehler beim laden der Entities!", e);
        } finally {
            em.close();
        }
    }

    @Override
    public List<VTeam> getVTeamsByUser(User user) throws ServiceException {

        EntityManager em = factory.createEntityManager();

        List<VTeam> vTeamListByUser = new ArrayList<VTeam>();

        if (user == null) {
            throw new ServiceException("Null User!");
        }

        try {

            vTeamListByUser =
                    em.createQuery("SELECT vt FROM VTeam vt JOIN UserVTeam uvt ON vt.id = uvt.vTeam "
                                   + "JOIN User u ON uvt.user = u.id WHERE u.id = '" + user.getId() + "'", VTeam.class)
                      .getResultList();

            return vTeamListByUser;

        } catch (IllegalStateException | IllegalArgumentException | PersistenceException e) {
            e.printStackTrace();

            throw new ServiceException("Fehler beim laden der Entities!", e);
        } finally {
            em.close();
        }
    }

    /**
     * Gibt alle Vteams in die der User noch nicht ist. Entfernt mir aus der Liste alle VTeams, welche der User schon
     * beigetreten ist
     */
    @Override
    public List<VTeam> getVTeamsUserNotJoined(User user) throws ServiceException {
        EntityManager em = factory.createEntityManager();

        if (user == null) {
            throw new ServiceException("Null User!");
        }

        try {
            List<VTeam> vTeamsUserNotJoined = getAllVTeams();
            List<VTeam> userVTeams = getVTeamsByUser(user); 
            for (int i = 0; i < vTeamsUserNotJoined.size(); i++) {
                for (int y = 0; y < userVTeams.size(); y++) {
                    if (vTeamsUserNotJoined.get(i).getId() == (userVTeams.get(y).getId())) {
                        vTeamsUserNotJoined.remove(i);
                        i = 0;
                        continue;
                    }
                }
            }

            return vTeamsUserNotJoined;

        } catch (IndexOutOfBoundsException | UnsupportedOperationException e) {
            e.printStackTrace();

            throw new ServiceException("Fehler beim laden der Entities!", e);
        } finally {
            em.close();
        }

    }

}
