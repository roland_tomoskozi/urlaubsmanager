package at.gv.brz.trainee.um.service.impl;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Service;

import static at.gv.brz.trainee.um.utility.Constants.PERSISTANCE_UNIT_NAME;
import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.UserVTeam;
import at.gv.brz.trainee.um.service.UserVTeamService;

/**
 * Serviceklasse für UserVTeam.
 * 
 * @author datzing
 *
 */
@Service
public class UserVTeamServiceJPA implements UserVTeamService {

    private static EntityManagerFactory entityManagerFactory =
            Persistence.createEntityManagerFactory(PERSISTANCE_UNIT_NAME);

    /**
     * Adden von einem UserVTeam.
     * 
     * @throws ServiceException
     */
    @Override
    public void add(UserVTeam userVTeam) throws ServiceException {
        if (userVTeam == null) {
            throw new ServiceException("UserVTeam ist null");
        }
        EntityManager em = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = null;

        try {
            transaction = em.getTransaction();
            transaction.begin();

            em.persist(userVTeam);
            em.close();

            transaction.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            em.close();
        }

    }

    /**
     * Setzt einen UserVTeam auf deleted.
     * 
     * @throws ServiceException
     */
    @Override
    public void delete(int id) throws ServiceException {
        if (id < 0) {
            throw new ServiceException("UserVTeam id ist negativ");
        }
        EntityManager em = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = null;

        try {
            transaction = em.getTransaction();
            transaction.begin();

            UserVTeam uvt = em.find(UserVTeam.class, id);
            uvt.setDeleted(true);
            uvt = em.merge(uvt);
            em.persist(uvt);

            transaction.commit();
        } catch (IllegalStateException | EntityExistsException | IllegalArgumentException | TransactionRequiredException
                | RollbackException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            em.close();
        }

    }

}
