package at.gv.brz.trainee.um.utility;

/**
 * Alle Konstanten für das Data.
 * 
 * @author gaalcs
 *
 */
public final class Constants {

    private Constants() {
        super();
    }

    public static final String PERSISTANCE_UNIT_NAME = "urlaubsmanagerPersisteneceUnit";
}
