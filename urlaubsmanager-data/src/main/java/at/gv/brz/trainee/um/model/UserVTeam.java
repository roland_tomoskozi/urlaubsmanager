package at.gv.brz.trainee.um.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity Klasse f�r die Verbindung von einem User und einem virtuellen Team.
 * 
 * @author schustefeli
 *
 */
@Entity
@Table(name = "user_vteam")
public class UserVTeam implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_user")
    private User user;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_vteam")
    private VTeam vTeam;
    
    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    
    /*
     * Getters && Setters
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public VTeam getvTeam() {
        return vTeam;
    }

    public void setvTeam(VTeam vTeam) {
        this.vTeam = vTeam;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
