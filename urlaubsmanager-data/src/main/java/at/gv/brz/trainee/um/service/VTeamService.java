package at.gv.brz.trainee.um.service;

import java.util.List;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.User;
import at.gv.brz.trainee.um.model.VTeam;

/**
 * Interface für die Methoden, um ein VTeam zu erstellen, löschen, updaten. Listen oder einzelne Daten davon zu
 * bekommen.
 * 
 * @author schustefeli
 *
 */
public interface VTeamService {

    /**
     * VTeam in der Datenbank hinzufügen.
     * 
     * @param vteam
     *            - vteam, bedeutet virtuelle Team.
     * @throws ServiceException
     */
    void addVTeam(VTeam vteam) throws ServiceException;

    /**
     * VTeam in der Datenbank aktualisieren.
     * 
     * @param vteam
     *            - vteam, bedeutet virtuelles Team.
     * @throws ServiceException
     */
    void updateVTeam(VTeam vteam) throws ServiceException;

    /**
     * VTeam auf gelöscht stellten.
     * 
     * @param id
     * @throws ServiceException
     */
    void deleteVTeam(Integer id) throws ServiceException;

    /**
     * Gibt eine Liste aller VTeams zurück.
     * 
     * @return vTeamList - vTeamList, bedeutet virtuelle Teamliste. Liste aller VTeams in der Datenbank
     * @throws ServiceException
     */
    List<VTeam> getAllVTeams() throws ServiceException;

    /**
     * Gibt eine Liste von VTeams eines bestimmen User zurück.
     * 
     * @param user
     * @return vTeamsByUserList - vTeamsByUserList, bedeutet virtuellen Teams By User List. Liste aller VTeams eines
     *         Users
     * @throws ServiceException
     */
    List<VTeam> getVTeamsByUser(User user) throws ServiceException;

    /**
     * GIbt eine Liste von VTeams zurück, welche der User noch nicht angehört.
     * 
     * @param user
     * @return vTeamsUserNotJoined - vTeamsUserNotJoined, bedeutet virtuellen Teams User Not Joined. Liste aller VTeams,
     *         denen der User noch nicht angehört
     * @throws ServiceException
     */
    List<VTeam> getVTeamsUserNotJoined(User user) throws ServiceException;

}
