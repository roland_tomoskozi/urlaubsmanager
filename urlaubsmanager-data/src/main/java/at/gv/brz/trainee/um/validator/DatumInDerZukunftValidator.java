package at.gv.brz.trainee.um.validator;

import com.sun.faces.util.MessageFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.Date;

/**
 * Überprueft, ob der Datum des Urlaubs in der Zukunft liegt. Wenn dieser Fall ist, wirft er eine Nachricht.
 *
 * @author pishbin
 *
 */
public class DatumInDerZukunftValidator implements Validator {
    public static final String DATUM_NICHT_IN_DER_ZUKUNFT_MSGID =
            "at.gv.brz.trainee.um.validator.DatumInDerZukunftValidator.DATUM_NICHT_IN_DER_ZUKUNFT_MSGID";

    /**
     * Überprueft, ob der Datum des Urlaubs in der Zukunft liegt. Wenn dieser Fall ist, wirft er eine Nachricht.
     */
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value == null) {
            return;
        }
        Date startDatum = (Date) value;

        Date now = new Date();

        if (!(doValidate(startDatum, now))) {
            throw new ValidatorException(MessageFactory.getMessage(DATUM_NICHT_IN_DER_ZUKUNFT_MSGID,
                                                                   FacesMessage.SEVERITY_ERROR));
        }
    }


	/**
	 * Hilfmethode, damit man den Validation leichter testen kann.
	 * 
	 * @param startDatum erste Tag der Urlaub
	 * @param now heutige Datum
	 * @return true wenn alles klappt.
	 */
	public boolean doValidate(Date startDatum, Date now) {
		if (startDatum == null) {
			return false;
		}

        if (now == null) {
            return false;
        }

        if (!(startDatum instanceof Date)) {
            return false;
        }

        if (startDatum.before(now)) {
            return false;
        }

        return true;
    }
}
