package at.gv.brz.trainee.um.service;

import java.util.List;
import java.util.Optional;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.Urlaub;

/**
 * Alle Methode f�r UrlaubService.
 * 
 * @author Nicole
 *
 */
public interface UrlaubService {

	void add(Urlaub urlaub) throws ServiceException;

	List<Urlaub> getUrlaubList() throws ServiceException;

	void update(Urlaub urlaub) throws ServiceException;

	Optional<Urlaub> get(int id) throws ServiceException;

	void delete(int id) throws ServiceException;

}
