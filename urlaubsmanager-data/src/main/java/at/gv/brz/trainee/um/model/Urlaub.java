package at.gv.brz.trainee.um.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.gv.brz.trainee.um.model.enums.Status;

/**
 * Entity Klasse f�r einen Urlaub.
 * 
 * @author schustefeli
 *
 */
@Entity
@Table(name = "urlaub")
public class Urlaub implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "start_datum", nullable = false)
    private Date startDatum;

    @Column(name = "end_datum", nullable = false)
    private Date endDatum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_user")
    private User user;

    @Column(name = "status", nullable = false, length = 255)
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "kommentar", nullable = true, length = 255)
    private String kommentar;

    @Column(name = "deleted", nullable = false, length = 255)
    private boolean deleted;

    /*
     * Getter && Setter
     */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDatum() {
        return startDatum;
    }

    public void setStartDatum(Date startDatum) {
        this.startDatum = startDatum;
    }

    public Date getEndDatum() {
        return endDatum;
    }

    public void setEndDatum(Date endDatum) {
        this.endDatum = endDatum;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Urlaub [id=" + id + ", startDatum=" + startDatum + ", endDatum=" + endDatum + ", user=" + user
               + ", status=" + status + ", kommentar=" + kommentar + ", deleted=" + deleted + "]";
    }
}
