package at.gv.brz.trainee.um.model.enums;

/**
 * Status Enum f�r einen Urlaub.
 * 
 * @author schustefeli
 *
 */
public enum Status {
    OFFEN, GENEHMIGT, ABGELEHNT;
}
