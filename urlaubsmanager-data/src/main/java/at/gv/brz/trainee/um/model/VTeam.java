package at.gv.brz.trainee.um.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity Klasse f�r ein virtuelles Team.
 * 
 * @author schustefeli
 *
 */
@Entity
@Table(name = "vteam")
public class VTeam implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "name", nullable = false, length = 255)
    private String name;
    
    @Column(name = "passwort", nullable = false, length = 255)
    private String passwort;
    
    @Column(name = "deleted", nullable = false)
    private boolean deleted;
    
    @OneToMany(mappedBy = "vTeam", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<UserVTeam> userVTeamListe;

    
    /*
     * Getters && Setters
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<UserVTeam> getUserVTeamListe() {
        return userVTeamListe;
    }

    public void setUserVTeamListe(List<UserVTeam> userVTeamListe) {
        this.userVTeamListe = userVTeamListe;
    }

    @Override
    public String toString() {
        return "VTeam [id=" + id + ", name=" + name + ", passwort=" + passwort + ", deleted=" + deleted + "]";
    }
}
