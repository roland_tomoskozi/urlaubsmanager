package at.gv.brz.trainee.um.model.enums;

/**
 * Rolle Enum f�r einen User.
 * 
 * @author schustefeli
 *
 */
public enum Rolle {
    MA, TL, PM, PL, ADMIN;
}
