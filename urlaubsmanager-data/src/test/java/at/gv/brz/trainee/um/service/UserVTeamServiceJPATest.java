package at.gv.brz.trainee.um.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.service.impl.UserVTeamServiceJPA;

/**
 * Testklasse für UserVTeamServiceJPA
 * @author datzing
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class UserVTeamServiceJPATest {
	
	@InjectMocks
	UserVTeamServiceJPA uvtS;
	
	@Test(expected = ServiceException.class)
	public void testNullObjectAdd() throws ServiceException
	{
		uvtS.add(null);
	}

	@Test(expected = ServiceException.class)
	public void deleteWithNegativeId() throws ServiceException
	{
		uvtS.delete(-123);
	}
}
