package at.gv.brz.trainee.um.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

/**
 * Überprüft, ob DatumInDerZukunftValidator richtig funktioniert.
 * 
 * @author pishbin
 *
 */
public class DatumInDerZukunftValidatorTest {

    private DatumInDerZukunftValidator datumInDerZukunftValidator = new DatumInDerZukunftValidator();

    private Date vergangenheit = new Date(1573426800000L);

    private Date zukunft = new Date(10442386800000L);

    private Date nowDate = new Date();

    /**
     * Überprüft, ob das Startdatum(in Vergangenheit) nicht vor dem heutigen Datum steht.
     */
    @Test
    public void testVergangenheit() {
        System.out.println(nowDate);
        assertFalse(datumInDerZukunftValidator.doValidate(vergangenheit, nowDate));
    }

    /**
     * Überprüft, ob das Startdate(in Zukunft) nicht vor dem heutigen Datum steht.
     */
    @Test
    public void testZukunft() {
        assertTrue(datumInDerZukunftValidator.doValidate(zukunft, nowDate));
    }

    /**
     * Überprüft, ob nowDate null ist.
     */
    @Test
    public void testNowDateIstNull() {
        assertFalse(datumInDerZukunftValidator.doValidate(zukunft, null));
    }

    /**
     * Überprüft, ob startDate null ist.
     */
    @Test
    public void testStartDatumIstNull() {
        assertFalse(datumInDerZukunftValidator.doValidate(null, nowDate));
    }
}
