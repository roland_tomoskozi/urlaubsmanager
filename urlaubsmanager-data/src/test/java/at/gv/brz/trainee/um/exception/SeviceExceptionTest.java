package at.gv.brz.trainee.um.exception;

import org.junit.Test;

/**
 * Testet, ob die ServiceException Klasse richtig funktioniert.
 *  
 * @author pishbin
 *
 */
public class SeviceExceptionTest {

	@SuppressWarnings("null")
	@Test(expected = NullPointerException.class)
	public void wannExceptionThrown_dannExpectationZufrieden() {
	    String test = null;
	    test.length();
	}
}
