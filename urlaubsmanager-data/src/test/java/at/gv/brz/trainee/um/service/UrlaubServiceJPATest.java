package at.gv.brz.trainee.um.service;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.service.impl.UrlaubServiceJPA;

@RunWith(MockitoJUnitRunner.class)
public class UrlaubServiceJPATest {

    @InjectMocks
    UrlaubServiceJPA us;

    @Test(expected = ServiceException.class)
    public void testNullObjectAdd() throws ServiceException {
        us.add(null);

    }

    @Test(expected = ServiceException.class)
    public void deleteWithNegativeId() throws ServiceException {
        us.delete(-12);

    }

    @Test(expected = ServiceException.class)
    public void getWithNegativId() throws ServiceException {
        us.get(-12);

    }

    @Test(expected = ServiceException.class)
    public void updatewithNull() throws ServiceException {
        us.update(null);
    }

}
