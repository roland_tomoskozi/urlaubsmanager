package at.gv.brz.trainee.um.service.impl;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.User;

/**
 * Eine Testklasse für UserServiceJPA, die hat eine main Methode. 
 * 
 * @author gaalcs
 */
public class UserServiceJPAWithMainTest {

    private static UserServiceJPA userServiceJPA = new UserServiceJPA();

    public static void main(String[] args) {

        getUserByEmailAndPassExistTest();
        getUserByEmailAndPassNotExistTest();
    }

    /**
     * User existiert nicht in der Datenbank, schreibt user null auf der Console aus.
     */
    private static void getUserByEmailAndPassNotExistTest() {
        try {
            System.out.println("\nUserServiceJPA getUserByEmailAndPassNotExistTest():\n");
            User user2 = userServiceJPA.getUserByEmailAndPass("csaba.gaal@brz.gv.atj", "Csaba123");
            if (user2 == null) {
                System.out.println("user null");
            } else {
                System.out.println(user2);
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }

    /**
     * User existiert in der Datenbank, schreibt user auf der Console aus.
     */
    private static void getUserByEmailAndPassExistTest() {
        try {
            System.out.println("\nUserServiceJPA getUserByEmailAndPassExistTest():\n");
            User user = userServiceJPA.getUserByEmailAndPass("csaba.gaal@brz.gv.at",
                                                             "c00136b70c622a2f9adacded824cb66d");
            if (user == null) {
                System.out.println("user null");
            } else {
                System.out.println(user);
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }
}