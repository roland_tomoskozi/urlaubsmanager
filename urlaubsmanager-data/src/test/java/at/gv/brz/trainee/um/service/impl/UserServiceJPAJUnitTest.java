package at.gv.brz.trainee.um.service.impl;

import org.junit.Ignore;
import org.junit.Test;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.User;
import org.junit.runner.RunWith;

/**
 * Die Testklasse für UserServiceJPA
 * 
 * @author gaalcs
 *
 */
@Ignore
public class UserServiceJPAJUnitTest {

    /**
     * Testet addUser() mit einem User mit null Wert hinzufügen.
     * @throws ServiceException
     */
    @Test(expected = ServiceException.class)
    public void addNullUserTest() throws ServiceException {
        User user = null;
        UserServiceJPA userServiceJPA = new UserServiceJPA();
        userServiceJPA.addUser(user);
    }

    /**
     * Testet addUser() mit einem User mit negativen Id hinzufügen.
     * @throws ServiceException
     */
    @Test(expected = ServiceException.class)
    public void addNegativeIdUserTest() throws ServiceException {
        User user = new User();
        user.setId(-1);
        UserServiceJPA userServiceJPA = new UserServiceJPA();
        userServiceJPA.addUser(user);
    }

    /**
     * Testet updateUser() mit einem User mit null Wert zu aktualisieren.
     * @throws ServiceException
     */
    @Test(expected = ServiceException.class)
    public void updateNullUserTest() throws ServiceException {
        User user = null;
        UserServiceJPA userServiceJPA = new UserServiceJPA();
        userServiceJPA.updateUser(user);
    }

    /**
     * Testet updateUser() mit einem User mit negativen Id aktualisieren.
     * @throws ServiceException
     */
    @Test(expected = ServiceException.class)
    public void updateNegativeIdUserTest() throws ServiceException {
        User user = new User();
        user.setId(-1);
        UserServiceJPA userServiceJPA = new UserServiceJPA();
        userServiceJPA.updateUser(user);
    }

    /**
     * Testet deleteUser() mit einem User mit null Wert zu löschen.
     * @throws ServiceException
     */
    @Test(expected = ServiceException.class)
    public void deleteNullUserTest() throws ServiceException {
        User user = null;
        UserServiceJPA userServiceJPA = new UserServiceJPA();
        userServiceJPA.deleteUser(user);
    }
    
    /**
     * Testet deleteUser() mit einem User mit negativen Wert zu löschen.
     * @throws ServiceException
     */
    @Test(expected = ServiceException.class)
    public void deleteNegativeIdUserTest() throws ServiceException {
        User user = new User();
        user.setId(-1);
        UserServiceJPA userServiceJPA = new UserServiceJPA();
        userServiceJPA.deleteUser(user);
    }

}
