package at.gv.brz.trainee.um.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import at.gv.brz.trainee.um.exception.ServiceException;
import at.gv.brz.trainee.um.model.User;
import at.gv.brz.trainee.um.model.VTeam;
import at.gv.brz.trainee.um.service.impl.VTeamServiceJPA;

/**
 * Test des VTeamServices.
 * 
 * @author schustefeli
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class VTeamServiceJPATest {

    /**
     * Testet, ob es eine Exception wirft, wenn der User null ist beim hinzufügen in der DB.
     * 
     * @throws ServiceException 
     */
    @Test(expected = ServiceException.class)
    public void testNullObjectAdd() throws ServiceException {
        VTeamServiceJPA vts = new VTeamServiceJPA();

        vts.addVTeam(null);

    }

    /**
     * Testet, ob es eine Exception wirft, wenn eine negative Id übergeben wird, beim User löschen.
     * 
     * @throws ServiceException 
     */
    @Test(expected = ServiceException.class)
    public void testDeleteWithNegativeId() throws ServiceException {
        VTeamServiceJPA vts = new VTeamServiceJPA();

        vts.deleteVTeam(-5);
        vts.deleteVTeam(50);

    }

    /**
     * Testet, ob es eine Exception wirft, wenn man mit einem Null Objekt updated.
     * 
     * @throws ServiceException 
     */
    @Test(expected = ServiceException.class)
    public void testUpdateWithNullObject() throws ServiceException {
        VTeamServiceJPA vts = new VTeamServiceJPA();

        vts.updateVTeam(null);

    }

    /**
     * Testet, ob es eine Exception wirft, wenn man Team mit null sucht.
     * 
     * @throws ServiceException 
     */
    @Test(expected = ServiceException.class)
    public void testGetVTeamByUserNull() throws ServiceException {
        VTeamServiceJPA vts = new VTeamServiceJPA();

        vts.getVTeamsByUser(null);

    }

    /**
     * Testet, ob man alle Teams aus der DB bekommt.
     */
    @Test
    public void getAllTest() {
        VTeamServiceJPA vts = new VTeamServiceJPA();

        List<VTeam> list;
        try {
            list = vts.getAllVTeams();

            for (VTeam t : list) {
                System.out.println(t);
            }

        } catch (ServiceException e) {
            e.printStackTrace();
        }

    }

    /**
     * Testet, ob man alle Teams eines bestimmten User bekommt.
     */
    @Test
    public void getVTeamByUserTest() {
        VTeamServiceJPA vts = new VTeamServiceJPA();

        List<VTeam> listByUser;

        User u = new User();
        u.setId(1);

        try {
            listByUser = vts.getVTeamsByUser(u);
            for (VTeam t : listByUser) {
                System.out.println(t);
            }

        } catch (ServiceException e) {
            e.printStackTrace();
        }

    }

}
