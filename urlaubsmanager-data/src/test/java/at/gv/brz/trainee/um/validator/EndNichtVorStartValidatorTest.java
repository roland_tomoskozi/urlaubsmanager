package at.gv.brz.trainee.um.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

/**
 * Ueberprueft, ob Enddate nicht vor Startdate steht.
 * 
 * @author toemoes
 *
 */
public class EndNichtVorStartValidatorTest {

    private EndNichtVorStartValidator endNichtVorStartValidator = new EndNichtVorStartValidator();

    private final String startDatum = "11.11.2019";

    private final String endDatum = "12.11.2019";

    private Date startDate = null;

    private Date endDate = null;

    /**
     * Konvertiert String to Typ Date.
     */
    @Before
    public void konvertStringToDate() {

        DateFormat formatter = new SimpleDateFormat("dd.mm.yyyy");

        try {
            startDate = formatter.parse(startDatum);
            endDate = formatter.parse(endDatum);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Überprüft, ob Enddate nicht vor Startdate steht.
     */
    @Test
    public void teststartDateVorEndDate() {
        assertTrue(endNichtVorStartValidator.doValidate(startDate, endDate));
    }

    @Test
    public void testEndDateVorStartDate() {
        assertFalse(endNichtVorStartValidator.doValidate(endDate, startDate));
    }

    @Test
    public void testEndDateIstNull() {
        assertFalse(endNichtVorStartValidator.doValidate(startDate, null));
    }

    @Test
    public void testStartDateIstNull() {
        assertFalse(endNichtVorStartValidator.doValidate(null, endDate));
    }
}