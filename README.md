## Maven Project check
If the IDEA does not recognize the project as a Maven project, you have to set it manually.
1. Click with right mouse on **pom.xml** than choose **add as maven project**.
---

## Check Tomcat Server
1. Click on **Add Configuration**.
2. Click on **plus symbol** then choose **Tomcat Server - Local** from the list.
3. Check Tomcat **Home Directory**!
4. **Fix Artifactory**.
---

## Prepare Database
1. Run **XAMPP** and start Apache and MySQL Servers
2. Click on the **Admin** button in the MySQL row
3. Create a new Database with name: **urlaubsmanagerdb**
4. Copy **urlaubsmanagerdb_v2.sql** (find under: Resources) in a text file and save it.
5. Import this file into urlaubsmanagerdb with **phpMyAdmin**.
---

## Connect to Database
1. Click **Database** on the right side.
2. Click on **plus symbol**, then on Data Source and select the MariaDB database.
3. Fill the following fields like this:
    1. **Name:** MariaDB
    2. **Host:** localhost
    3. **Port:** 3306
    4. **User:** root
    5. **URL:** jdbc:mariadb://localhost:3306
4. **Test** the connection
---

## Check entity
1. Select an entity and check if the name of the column/table has the correct data soruce.
   If they are not correct, change them.
---

## Maven
1. Click **Maven** on the right side.
2. Select your maven project than click on **Lifecycle**.
3. Select **clean** and **install** from the list and **run** it.
---

## Run the application
1. Click on the **green triangle**
2. Log in:
    1. **User:** 
    2. **Password:** 
